<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\SearchPlugin\Finder;

use Omni\Sylius\SearchPlugin\Model\SearchIndexInterface;
use Pagerfanta\Pagerfanta;
use Sylius\Component\Resource\Model\ResourceInterface;
use Symfony\Component\HttpFoundation\Request;

interface FinderInterface
{
    /**
     * @param SearchIndexInterface[]|\Iterator $indexes
     *
     * @return ResourceInterface[]
     */
    public function find(\Iterator $indexes): array;

    /**
     * @param Request $request
     *
     * @return Pagerfanta
     */
    public function getPager(Request $request): Pagerfanta;
}
