<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\SearchPlugin\Model;

use Sylius\Component\Resource\Model\ResourceInterface;

interface SearchIndexInterface extends ResourceInterface
{
    /**
     * @param int $itemId
     *
     * @return SearchIndexInterface
     */
    public function setResourceId($itemId);

    /**
     * @return int
     */
    public function getResourceId();

    /**
     * @param string $entity
     *
     * @return SearchIndexInterface
     */
    public function setResourceClass($entity);

    /**
     * @return string
     */
    public function getResourceClass();

    /**
     * @param string $value
     *
     * @return SearchIndexInterface
     */
    public function setIndex($value);

    /**
     * @return string
     */
    public function getIndex();

    /**
     * @param \DateTime $createdAt
     *
     * @return SearchIndexInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * @return \DateTime
     */
    public function getCreatedAt();
}
