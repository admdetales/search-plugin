<?php

namespace Omni\Sylius\SearchPlugin\Twig;

use Sylius\Component\Resource\Model\ResourceInterface;

class IndexExtension extends \Twig_Extension
{
    /**
     * @var array
     */
    private $config;

    /**
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('omni_sylius_search_get_route', [$this, 'getRoute']),
        ];
    }

    /**
     * @param ResourceInterface $resource
     *
     * @return string
     */
    public function getRoute(ResourceInterface $resource): ?string
    {
        foreach ($this->config as $index) {
            if (get_class($resource) == $index['class']) {
                return $index['route'];
            }
        }

        return null;
    }
}
